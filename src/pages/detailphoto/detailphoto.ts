import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhotoServiceProvider } from "../../providers/photo-service/photo-service"
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the DetailphotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailphoto',
  templateUrl: 'detailphoto.html',
})
export class DetailphotoPage {
	photo: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public photoSvc : PhotoServiceProvider) {
  	this.photo = this.navParams.get("photo");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailphotoPage');
  }

  votePhoto(id){
  	this.photoSvc.votePhoto(id).subscribe(data => {
      alert('Voto computado!');
    }, error => { alert('Erro ao computar voto!'); console.log(error)});
  }

}
