import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailphotoPage } from "../detailphoto/detailphoto"
import { PhotoServiceProvider } from "../../providers/photo-service/photo-service"

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
	photos: any = [];

  constructor(public navCtrl: NavController, public photoSvc: PhotoServiceProvider) {
    this.photoSvc.getPhotos()
      .subscribe(data => {this.photos = data; this.photos = this.photos.sort(this.sortByVotes).reverse();},
                 err => console.log('Could not fetch photos: ' + err)
      )
    this.photos = this.photos.sort(this.sortByVotes);
  }

  ionViewLoaded(){
    if(this.photos != []){
      this.photoSvc.getPhotos()
        .subscribe(data => {this.photos = data; this.photos = this.photos.sort(this.sortByVotes).reverse();},
                   err => console.log('Could not fetch photos: ' + err)
        )
    this.photos = this.photos.sort(this.sortByVotes);
    }
  }

  photoSelected(photo){
  	this.navCtrl.push(DetailphotoPage, {
  		photo: photo
  	});
  }

  sortByVotes(p1, p2){
    if(p1.votes > p2.votes){
      return 1;
    } else if(p1.votes < p2.votes){
      return -1;
    } else {
      return 0;
    }
    //return (p1.votes > p2.votes) ? 1 : (p1.votes < p2.votes) ? -1 : 0;
  }

}
