import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the PhotoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PhotoServiceProvider {
	photos: any;

  constructor(public http: HttpClient) {
  }

  getPhotos(){
    return this.http.get('http://localhost:3000/photos');
  }

  votePhoto(id){
    return this.http.get('http://localhost:3000/photos/' + id + '/vote');
  }
}
  	// return new Promise(resolve => {
  	// 	this.http.get('http://localhost:3000/photos')
  	// 		.subscribe(data => {
  	// 			this.photos = data.json();
  	// 			resolve(this.photos);
  	// 		});
  	// });
